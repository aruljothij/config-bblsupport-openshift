# -------------------------------------------------------------------------------------------
# NOTE : 
# 	This should be used only for DEV instance
# 	For Live instance, all variables should be defined as Environment variables.
# 	Environment variables hold precedence against what is defined in this file
# -------------------------------------------------------------------------------------------

# Allow local configuration to override Remote Externalized configuration
spring.cloud.config.allowOverride=true
# But, only System properties or Env variables (and not local config files) will override externalized configuration
spring.cloud.config.overrideSystemProperties=false



#############################################################################################
###### Global App Specific Configuration
#############################################################################################
# Logging level (DEBUG,INFO,WARN,FATAL)
logging.level.ROOT=INFO
# to ensure errors logged by Camel RabbitMQConsumer only if severity is ERROR (it helps avoiding lot of noise at WARNING level)
logging.level.org.apache.camel.component.rabbitmq.RabbitMQConsumer=ERROR

# Used by Spring Boot (Application Context)
server.contextPath=/igtb-groups

# Port 51002
server.port=51002

# Module Abbreviation Name (<= 10 characters and only Upper Case Alphabets A-Z)
# e.g. LMS (for Liquidity), IPSH (for Payments), CNR (for Collections & Receivables)...
# This is used for deriving:
# 	- Redis Key name as action-requests:<moduleAbbr>:${channelSeqId} (e.g. action-requests:lms:10581856258-94299189, action-requests:ipsh:..., action-requests:cnr:....)
ModuleAbbr=GRPS

# Header key and value, to be used as security token while calling APIs for this module
iGTBD-AtomicAPI-SharedKey=I4qwGynNIt5DP5zUjjemHR1mEj8Ii6jq

# Redis Database connection details
Redis.DB.Url=localhost:6379
Redis.DB.Password=
# if SSL set to true, Redis client will use standard JRE truststore for SSL connectivity
Redis.DB.SSL.Enabled=true
# "true" indicates hostname mistmatch exception should be avoided (test env specific)
#Redis.DB.SSL.DisableHostNameVerification=false

## For Sentinel connection
#Redis.Sentinel.Enabled=true
#Redis.Sentinel.Urls=52.30.51.155:26379,34.242.88.42:26379,34.252.191.116:26379
#Redis.DB.Master.Name=redis-master
#Redis.DB.Password=redis-password
#Redis.DB.Instance=0
#Redis.Sentinel.FailoverWaitTimeMS=10000

# To Enable/Disable DevMsg field in JSON response returned from API
# Y - enables DevMsg in output json response (may be useful for dev env)
# N - disables the same (recommended for production env) (this is default, if not specified)
EnableDevMsgInResponse=N

# Camel Messages to be logged at this level (Possible values: ERROR, WARN, INFO, DEBUG, TRACE, OFF)
CamelMessageLoggingLevel=DEBUG

# Release Batch Size
# It is max number of requests to be released per batch (Approved, Retry batch)
ReleaseBatchSize=200

# Represents DataCenter region, country the process events/data belongs to
DataCenter.Region=
DataCenter.Country=

# Represents comma separated multiple service key regex patterns (product/subproduct), this module is supposed to process events/data related to
Digital.ServiceKey.Patterns=group/action:add




##################################################################
### Zipkin specific options
##################################################################
spring.zipkin.enabled=false
spring.sleuth.enabled=false
#spring.sleuth.web.additionalSkipPattern=.*/getDetails|.*/updateDetails
#spring.sleuth.sampler.probability=1.0
#spring.zipkin.baseUrl=http://localhost:9411





##################################################################
### Message Broker configuration
##################################################################
# Message Broker which is to be used by Action API Commons for handling triggers/events
# Supported values - RabbitMQ only
Digital.MsgBroker.Type=RabbitMQ
# This is used by Commons for connecting to RabbitMQ Server
Digital.RabbitMQ.Host=localhost
Digital.RabbitMQ.Port=5672
Digital.RabbitMQ.User=guest
Digital.RabbitMQ.Password=guest
Digital.RabbitMQ.VHost=/
# if SSL set to true, RMQ client will use standard JRE truststore for SSL connectivity.
# internally creates rmqTrustManager bean as default TrustManager for Camel producer/consumer endpoints
Digital.RabbitMQ.SSL.Enabled=true

Digital.RabbitMQ.ConcurrentConsumers=3
Digital.RabbitMQ.ThreadPoolSize=10
Digital.RabbitMQ.ConnFactory=
Digital.RabbitMQ.Exchange.Event=cbxevents
Digital.RabbitMQ.Exchange.EvtDL=cbxevents
#Digital.RabbitMQ.Queue.Release=action-groups.release
Digital.RabbitMQ.Queue.EvtDL=dlq.action-groups

Digital.RabbitMQ.Exchange.Release=cbx.wflw.init
Digital.RabbitMQ.Exchange.RelDL=cbx.wflw.init
#Digital.RabbitMQ.Queue.Release=action-groups.release
Digital.RabbitMQ.Queue.RelDL=dlq.action-groups.rel
## WARNING=if you change the routing key bindings, you must MANUALLY remove old bindings from exchange

Digital.RabbitMQ.camelComponent=rabbitmq
Digital.RabbitMQ.camelComponent.options=prefetchEnabled=true&prefetchCount=30
Digital.RabbitMQ.camelMsgLoggingLevel=DEBUG
# Used as sleep time (in milliseconds) between each message retry, when msg is requeue'd.
# Keep it commented, unless need to override default value.
#MsgRetrySleepTime=250

# Used as max time (in seconds) a message to be retried for in requeue mode
# Keep it commented, unless need to override default value.
#MsgRetryMaxTime=86400





#############################################################################################
###### Release Retry Configuration
#############################################################################################
RelRetryCfgIdentifierKeys=GROUPREQUESTLIST_ANY

###
# Release Retry Config Identifier key based configuration
# 
# RelRetryCfgIdentifier.<identifierKey>.payloadType=
# RelRetryCfgIdentifier.<identifierKey>.requestType=
# RelRetryCfgIdentifier.<identifierKey>.maxReleaseRetry=
# Where,
#	<payloadType,requestType> together forms a unique key combination for providing related configuration 
#	payloadType = type of the payload e.g. SweepStructure, LoanAgreement or any
#	requestType = type of the request e.g. create, update, delete or any
# 	maxReleaseRetry = to indicate how many times this message should be retried for releasing before it is marked as Failed
#			(field is optional - default value will be applied)
#
# Configuration is applied in following order of priority, for a combination of <payloadType> and <requestType>:
# 1. <payloadType> and <requestType> exactly match with values provided here.
# 2. Else - <payloadType> and "any"
# 3. Else - "any" and <requestType>
# 4. Else - "any" and "any"
# 5. Else - default retry configuration is applied
#
###
RelRetryCfgIdentifier.GROUPREQUESTLIST_ANY.payloadType=GroupRequestList
RelRetryCfgIdentifier.GROUPREQUESTLIST_ANY.requestType=any
RelRetryCfgIdentifier.GROUPREQUESTLIST_ANY.maxReleaseRetry=

### Transformer routes
# GROUPREQUESTLIST specific
RelConnector.GROUPREQUESTLIST_TXFMR.route=direct:TransformGroupRequestListRoute


### Backend Endpoints
# GROUPREQUESTLIST specific
# Endpoint using JMS
#RelConnector.GROUPREQUESTLIST_JMS.endpoint=INSTANCE.1.AMQ.Component:topic:Release-UserQ?disableReplyTo=true
RelConnector.GROUPREQUESTLIST_REL.route=direct:ReleaseGroupRequestPayloadRoute


### Routing Slips
# GROUPREQUESTLIST specific
RelConnector.GroupRequestListCreate.routingSlip=${ValConnector.GROUPREQUESTLIST_VAL.route},\
${RelConnector.GROUPREQUESTLIST_TXFMR.route},\
${RelConnector.GROUPREQUESTLIST_REL.route}
RelConnector.GroupRequestListAny.routingSlip=${ValConnector.GROUPREQUESTLIST_VAL.route},\
${RelConnector.GROUPREQUESTLIST_TXFMR.route},\
${RelConnector.GROUPREQUESTLIST_REL.route}

### Backend Endpoints
# GROUPREQUESTLIST specific
# Validation Endpoint using Interface approach
#ValConnector.GROUPREQUESTLIST_HTTP.endpoint=jetty://http://localhost:51003/stubs/validators/v1/user
#ValConnector.GROUPREQUESTLIST_HTTP.endpoint=direct:ValidateStub
ValConnector.GROUPREQUESTLIST_VAL.route=bean:validateGroupRequestListPayloadBean?method=validateRequestPayload


### Routing Slips
# GROUPREQUESTLIST specific
ValConnector.GroupRequestListCreate.routingSlip=${ValConnector.GROUPREQUESTLIST_VAL.route}
ValConnector.GroupRequestListAny.routingSlip=${ValConnector.GROUPREQUESTLIST_VAL.route}


#############################################################################################
###### Release Trigger Handler specific Configuration
#############################################################################################
# Name of the Apache Camel Component,
# which is to be used by Commons package for subscribing/publishing to Release Batch Trigger events 
RelTriggerHandler.CamelComponent=rabbitmq
RelTriggerHandler.CamelComponent.Options=prefetchEnabled=true&prefetchCount=30

# PayloadTypes - comma separated list of Resources, for which this module need to support running Release Batches
RelTriggerHandler.PayloadTypes=GroupRequestList





#################################################################################################
###### State Update Handler specific Configuration
#################################################################################################
# Name of the Apache Camel Component,
# which is to be used by Commons package for subscribing/publishing to B/E State Update events 
StateUpdHandler.CamelComponent=rabbitmq
StateUpdHandler.CamelComponent.Options=prefetchEnabled=true&prefetchCount=30





#################################################################################################
###### Event Publish Handler specific Configuration
#################################################################################################
# Name of the Apache Camel Component,
# which is to be used by Commons package for publishing Events / Triggers
MsgPubHandler.CamelComponent=rabbitmq
#MsgPubHandler.CamelComponent.Options=





#############################################################################################
###### Purge Trigger Handler specific Configuration
#############################################################################################
# Name of the Apache Camel Component (which is configured at Domain level),
# which is to be used by Commons package for subscribing/publishing to Purge Batch Trigger events 
PurgeTriggerHandler.CamelComponent=rabbitmq
PurgeTriggerHandler.CamelComponent.Options=prefetchEnabled=true&prefetchCount=30

# Minimum number of days the data must be retained in State Store (irrespective of what is specified in PurgeStateStoreTrigger)
PurgeTriggerHandler.MinRetentionDays=720
# Flag indicating whether purging of IN_PROGRESS requests from State Store is allowed or not (irrespective of what is specified in PurgeStateStoreTrigger)
PurgeTriggerHandler.PurgeOfInProgressAllowed=false


# Comma separated list of Payload Types, for which this module need to support running Purge Batches
PurgeTriggerHandler.PayloadTypes=GroupRequestList
PublishBackendEvents=true

#############################################################################################
###### Registering mgmt endpoints to Eureka
#############################################################################################
eureka.instance.statusPageUrlPath=${server.contextPath}${management.context-path}/info
eureka.instance.healthCheckUrlPath=${server.contextPath}${management.context-path}/health



#############################################################################################
### Quest specific parameters 
#############################################################################################
### Feign target client name
### IF "digital-gatekeeper", then service will be resolved via Eureka service discovery (by default)
### IF "quest-direct", then service will be connected directly using URIs (in quest-direct.ribbon.listOfServers)
digital.quest.feignClientName=digital-gatekeeper
        
### Quest URI (IF using Gkp then "/quest/graphql". IF using Quest directly then "/graphql")
digital.quest.uri=/quest/graphql

### To provide details, when connecting with Quest using direct Host/IP (non-Eureka approach)
quest.url=http://digital-quest-bblsupport.openshift-devops-ops02-ca59b22659737722f226d84b287c7ed8-0000.us-south.containers.appdomain.cloud
quest-direct.ribbon.listOfServers=${quest.url}
quest-direct.ribbon.eureka.enabled=false

ribbon.ConnectTimeout=20000
ribbon.ReadTimeout=20000
#############################################################################################



###############################################################################
######## allowed functions, actions, subActions and workflow actions ##########
###############################################################################

allowed.functions=add,amend,delete,hold,unhold,suspend,resume,exec,add_si,amend_si,create_batch,delete_si,hold_si,unhold_si,add_template,amend_template,delete_template,upload,obo_access,generate,upload_bulk,addlvl,amendlvl,closelvl,initpy,amndpy,cnclpy,initpr,cnclpr,initrf,pyrem,retry
allowed.actions=draft,initiate,validate,trash,draft_si,initiate_si,validate_si,release,authorize,verify,lock,release_si,authorize_si,verify_si,lock_si,authorize_template,trash_template,fileUpload,fileVerify,fileInitiate,fileRelease,fileAuthorize,fileTrash,batchVerify,batchRelease,batchAuthorize,batchTrash
allowed.workflow.actions=release,authorize,verify,trash,lock,release_si,authorize_si,verify_si,lock_si,authorize_template,trash_template,fileVerify,fileRelease,fileAuthorize,fileTrash,batchVerify,batchRelease,batchAuthorize,batchTrash
allowed.actions.subActions={ \
  'lock': {'lock','release'}, \
  'release': {'confirm','reject'}, \
  'authorize': {'confirm','reject'}, \
  'verify': {'confirm','reject'}, \
  'trash': {'confirm'}, \
  'lock_si': {'lock','release'}, \
  'release_si': {'confirm','reject'}, \
  'authorize_si': {'confirm','reject'}, \
  'verify_si': {'confirm','reject'}, \
  'authorize_template': {'confirm','reject'}, \
  'trash_template': {'confirm'}, \
  'fileVerify': {'confirm','reject'}, \
  'fileAuthorize': {'confirm','reject'}, \
  'fileTrash': {'confirm'}, \
  'fileRelease': {'confirm','reject'}, \
  'batchVerify': {'confirm','reject'}, \
  'batchAuthorize': {'confirm','reject'}, \
  'batchTrash': {'confirm'}, \
  'batchRelease': {'confirm','reject'} \
  }
 
#################################################################################################
### State Management controls for draft and initiate states for unhappy validation scenarios
#################################################################################################
# Flag to indicate if initiate request should be marked as failed (500) if validate call gets timed out. Defaults to false. 
StateMgmt.failInitiateOnValTimeout=true
# Flag to indicate if original draft should be retained or not, if validate call gets timed out during draft-2-initiate transition. Defaults to false. 
StateMgmt.retainDraftOnValTimeout=true
# Flag to indicate if original draft should be retained or not, if there are validation failure (400,422) during draft-2-initiate transition. Defaults to false. 
StateMgmt.retainDraftOnValFailure=true
#################################################################################################

#############################################################################################
### Exception handling specific log configuration 
#############################################################################################
### Log level-step-up configuration (LEVEL1:Seconds,LEVEL2:Seconds...)
### Keep it commented, unless there is need to customize
#logging.levelStepUp.CBXS_SYS_BE_REL_RETRY=WARN:0,ERROR:120
#logging.levelStepUp.CBXS_ACT_COM_BE_EVT_REQUEUE=INFO:0,WARN:5,ERROR:120

### DLQ specific action_code's
### Keep it commented, unless there is need to customize
#logging.dlqActionCode.CBXS_SYS_MSG_INVALID=INV_MSG
#logging.dlqActionCode.CBXS_SYS_RETRY_EXCEEDED=OFFLINE
#############################################################################################