# -------------------------------------------------------------------------------------------
# NOTE : 
# 	This should be used only for DEV instance
# 	For Live instance, all variables should be defined as Environment variables.
# 	Environment variables hold precedence against what is defined in this file
# -------------------------------------------------------------------------------------------

# Allow local configuration to override Remote Externalized configuration
spring.cloud.config.allowOverride=true
# But, only System properties or Env variables (and not local config files) will override externalized configuration
spring.cloud.config.overrideSystemProperties=false



#############################################################################################
###### Global App Specific Configuration
#############################################################################################
# Logging level (DEBUG,INFO,WARN,FATAL)
logging.level.ROOT=INFO

# Used by Spring Boot (Application Context)
server.contextPath=/igtb-limits

#application name
applicationName=digital-limits-0.3.9

# Port 51002
server.port=51001

# Module Abbreviation Name (preferably <= 3 characters and only Upper Case Alphabets A-Z)
# e.g. LMs (for Liquidity), IPSH (for Payments), CNR (for Collections & Receivables)...
# This is used for deriving:
# 	- ElasticSearch Index name as <moduleAbbr>-requests (e.g. lms-requests, ipsh-requests, cnr-requests)
#	- Deriving Destination names in Release Trigger and State Update handler modules
ModuleAbbr=LMT

# ES Database connection details
ES.DB.Protocol=https
ES.enrichment.DB.Host=elasticsearch.bbl-support.ibm.igtb.digital
ES.enrichment.DB.Port=443
ES.enrichment.DB.User=cbx-app
ES.enrichment.DB.Password=s3cr3t
ES.DB.SchemaVersion = 7
ES.DB.SSL.DisableHostNameVerification=true

# Redis Database connection details
#Redis.DB.Host=redis.bbl-support.ibm.igtb.digital
Redis.DB.Host=10.240.64.8
Redis.DB.Port=6378
Redis.DB.User=
Redis.DB.Password=
Redis.DB.SSL.Enabled=true
Redis.DB.SSL.DisableHostNameVerification=true

redisRetryIntervalInMs=250

#################################################################

events.region=europe
events.country=DNK

Digital.JmsBroker.Host=ec2-34-229-14-224.compute-1.amazonaws.com
Digital.JmsBroker.Port=5671
Digital.JmsBroker.User=cbx-app
Digital.JmsBroker.Password=secr3t
Digital.JmsBroker.VHost=/

# main exchange where all events are published
limits.rabbitmq.exchange = cbxevents

# rabbitMq Conumer error 
logging.level.org.apache.camel.component.rabbitmq.RabbitMQConsumer=ERROR

#### Zipkin ###
spring.zipkin.enabled=false
spring.sleuth.enabled=false


# To Enable/Disable DevMsg field in JSON response returned from API
# Y - enables DevMsg in output json response (may be useful for dev env)
# N - disables the same (recommended for production env) (this is default, if not specified)
EnableDevMsgInResponse=N

######################################################################################################
# This property will define whether per day limits will be calculated  using action date or auth date
# POSSIBLE VALUE - ACTION_DATE 
# POSSIBLE VALUE - TRANSACTION_DATE 
######################################################################################################
VALIDATE_DAILY_LIMITS_BY = TRANSACTION_DATE

######################################################################################################################
# This property will define whether on limit validation failure in rule it should proceed executing other rules or not.
# POSSIBLE VALUE - Yes
# POSSIBLE VALUE - No 
######################################################################################################################
BREAK_ON_FAILURE = No

###########################################################################################################
# This property will defines the rule order key in which the initiation limits execution order is defined.
###########################################################################################################
limit_initiation_rules = InitPerTxnAmtLimitValidationForUser,InitCsldtTxnCountLimitValidationForUser,InitTxnCountLimitValidationForUser,InitCsldtTxnCountLimitValidationForCorporate,InitTxnCountLimitValidationForCorporate,InitCsldtTxnAmtLimitValidationForUser,InitTxnAmtLimitValidationForUser,InitCsldtTxnAmtLimitValidationForCorporate,InitTxnAmtLimitValidationForCorporate,InitProductLimitValidation

########################################################################################################
# This property will defines the rule order key in which the approval limits execution order is defined.
########################################################################################################
limit_approval_rules = PerTransactionApproverLimitValidation,PerTransactionJointApproverLimitValidation,PerDayApprovalCsldtLimitValidation,PerDayApprovalLimitValidation,PerDayApprovalCsldtLimitValidationForCorporate,PerDayApprovalLimitValidationForCorporate,ProdPerDayApprovalLimitValidation

#Validate Incoming / Outgoing Limits based on the levels specified
incoming_limits_level = Service
outgoing_limits_level = Corporate,User,Service
######################################################################################################
# This property will define whether approval limits at corporate and product level should be consumed 
# for first approver alone or at each approver approves transaction limits should be consumed at all level
# POSSIBLE VALUE - FIRSTAPPR (First Approver)  
# POSSIBLE VALUE - ALL (For All Approvers)
######################################################################################################
consume_approval_limits = FIRSTAPPR

###########################################################################################################
# This property will defines the rule order key in which the product level limits should be found.
###########################################################################################################
product_rules = ProductUserLevelLimits,ProductUserLevelAllCurrencyLimits,ProductLevelLimits,ProductLevelAllCurrencyLimits,ProductEntityLevelLimits,ProductEntityLevelAllCurrencyLimits,ProductDomainLevelLimits,ProductDomainLevelAllCurrencyLimits,ProductAllEntityUsersLimits,ProductAllEntityUsersAllCurrencyLimits

###########################################################################################################
# This property will defines the rule order in which the second approver product level limits should be get
###########################################################################################################
second_approver_product_rules = ProductUserLevelLimits,ProductUserLevelAllCurrencyLimits,ProductEntityLevelLimits,ProductEntityLevelAllCurrencyLimits

###########################################################################################################
# This property will defines the rule order key in which the user level limits should be found.
###########################################################################################################
user_rules= UserEntityLevelLimits,UserDomainLevelLimits

###########################################################################################################
# This property will defines the rule order key in which the corporate level limits should be found.
###########################################################################################################
corporate_rules = DomainEntityLevelLimits,DomainLevelLimits

#####################################################################################
# This property will defines the package in which the rule classes are located and loaded.
#####################################################################################
rules_package = com.igtb.api.action.lmt.rules

#####################################################################################
# This property will defines the rule order criteria.
#####################################################################################
rules_order = name

#############################################################################################
# This property will defines the properties file name in which the rule order key is defined.
#############################################################################################
rules_config_filename = application.properties

#######################################################
#Client ID of the digital gatekeeper in eureka registry
#######################################################
digital_gatekeeper_eurekaId=digital-gatekeeper

######################################################
#Client ID of the digital gazetteer in eureka registry
######################################################
digital_gazetteer_eurekaId=digital-gazetteer

###########################
#Digital Quest Inquiry Path 
###########################
quest_url=/quest/graphql

#######################
#Gazetteer Inuqiry Path
#######################
gazetteer_url=/gazetteer/graphql

##############################
#Workflow History Inquiry Path 
##############################
workflow_history_url=/approvalwf/igtb-approvalwf/v1/viewWorkflowHistory

###########################################################################################
#This property define no of days for utilized limits for a transaction should be maintained
###########################################################################################
limits_Expiry=7


#############################################################################################
# This property will defines format of the date.
#############################################################################################

Date_Format=yyyy-MM-dd

PREFIX_NAME=limitsconfig:

##############################################################
#shared secret for system inquiries (within the delivery tier)
x-shared-secret=6c3d57fe-cb79-46d0-945d-c2f8e30120c8
##############################################################

IS_AUDIT_EVENT_ENABLED=true

#Payload with multiple sub products following are the request types
multi_prod_file_upload = upload

#Reference for transaction request type.
action_initiation=initiate,add,amend,add_si,amend_si,upload,upload_bulk,create_batch,create_batch_bulk

#Reference for transaction type as file upload.
action_approval=approve,authorize,authorize_si

#Reference for approval action.
action_transaction=transaction,authorization

#Reference for initiation action.
action_fileupload=filetransaction,upload

#Reference for revoke
action_revoke=reject,trash,verify,release

#Reference for revoke limits for certain status of transaction
transaction_status = request_rejected

#Reference for executing standing instructions (context.action#context.type)
si_actions_type= add_si#execute_si

#Reference for validating limits for the standing instruction execution
si_limit_validation_actions = initiate,add_si,approve,authorize_si 

bulk_actions_type = create_batch_bulk,upload_bulk

###########################################################################################
#This property define the default lock prefix key
###########################################################################################

lock_prefix=lock: